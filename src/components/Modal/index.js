import {
  Paragraph,
  Modal,
  Img,
  Shadow,
  X
} from './styled'

export default function ModalCard ({ visible, selectedProjectId, onCloseModal }) {
  return (
    <Shadow hidden={visible}>
      <Modal>
        <X><Img src='img/icon-close.svg' onClick={onCloseModal} /> </X>
        <Paragraph>{selectedProjectId.name}</Paragraph>
        
      </Modal>
    </Shadow>
  )
}
