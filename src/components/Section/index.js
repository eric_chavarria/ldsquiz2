import {
  Container,
  Paragraph,
  Title,
  Logo,
  CardContainer,
  Card
} from './styled'

export default function Section ({ jdata, onShowModal }) {
  return (
    <Container >
      <CardContainer size={jdata.length}>
        {
          jdata.map(item => (
            <Card key={item.id} image={'fill2.jpg'} onClick={onShowModal(jdata[item.id -1])}>
              <Paragraph name={item.id}>
                {item.id}
              </Paragraph>
            </Card>
          ))
        }
      </CardContainer>
    </Container>
  )
}
