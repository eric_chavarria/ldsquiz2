import { useState, useCallback } from 'react'
import Section from '../components/Section/index'
import Modal from '../components/Modal/index'
import {
  Bullet,
  Projects
} from '../styled'

function Home (Info) {
  const [hidden, setHiden] = useState('hidden')
  const [element, setElement] = useState('')
  const click = useCallback((number) => {
    return () => {
      setHiden('visible')
      setElement(number)
    }
  }, [])
  function closeModal () {
    setElement('')
    setHiden('hidden')
  }
  return (
    <>
      
      <Projects>
        <Bullet>Preguntas</Bullet>
        <Section onShowModal={click} jdata={Info.Info.MOBILE}  />
        <Modal visible={hidden} selectedProjectId={element} onCloseModal={closeModal} />
      </Projects>
    
    </>
  )
}

export default Home
export async function getServerSideProps () {
  const res = await fetch(process.env.Vercel_URL + '/api/info-pages')
  const Info = await res.json()
  return {
    props: {
      Info
    }
  }
}
